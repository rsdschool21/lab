import time

from coapthon.client.helperclient import HelperClient
from locust import User, task, events, constant

import os
import random

class COAPClient(HelperClient):
    def __init__(self, server):
        import gevent
        super().__init__(server=server)
        self.queue = gevent.queue.Queue()

    def get(self, path):
        # wrapper around the get request of the coap library
        # to get the timing information and errrors reports
        host, port = self.server
        name = f"[{host}]:{port}{path}"
        start_time = time.time()
        try:
            result = None
            result = super().get(path)
        except Exception as e:
            total_time = int((time.time() - start_time) * 1000)
            events.request_failure.fire(request_type="coapclient", name=name, response_time=total_time, exception=e, response_length=0)
        finally:
            total_time = int((time.time() - start_time) * 1000)
            events.request_success.fire(request_type="coapclient", name=name, response_time=total_time, response_length=0)
        return result


class MyUser(User):

    wait_time = constant(1)

    def __init__(self, environment):
        super().__init__(environment)
        # COAP_NODES = "2001:660:5307:3142::b468,2001:660:5307:3142::a770,2001:660:5307:3142::9181"
        # first, deconstruct the target nodes
        servers = os.environ["COAP_NODES"].split(",")
        
        # then, pick one and instantiate a coap client
        self.client = COAPClient(server=(random.choice(servers), 5683))
    
    @task
    def get_gyros(self):
        self.client.get("/sensors/gyros")