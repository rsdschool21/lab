# RSD - Hackathon

Repository of the RSD Autumn School - Hackathon.

General organization:

- **analysis**: set of notebooks used in the experiments

  Since each experiment may need specific analysis, we also keep in git the output notebooks.

- **data**: used to save raw output from experiments (debug, files generated or collected from testbeds)

  The amount of data may be large and we opt for not committing them to the git repository.
  
  *git-annex* can be used to keep track of these files without adding them to the main git repository.

- **docker**: file to build container image

- **setup**: some notebooks describing the initial setup to access the testbeds and compile the firmwares.

- **src**: other source files needed to run the experiments (program, libs, etc)
